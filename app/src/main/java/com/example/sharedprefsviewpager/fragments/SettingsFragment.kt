package com.example.sharedprefsviewpager.fragments

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatDelegate
import androidx.fragment.app.Fragment
import com.example.sharedprefsviewpager.R
import com.google.android.material.button.MaterialButtonToggleGroup

class SettingsFragment: Fragment (R.layout.fragment_settings) {

    private lateinit var themeButton: MaterialButtonToggleGroup

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val sharedPreferences = activity?.getSharedPreferences("Theme", Context.MODE_PRIVATE)
        val editor = sharedPreferences?.edit()

        themeButton = view.findViewById(R.id.themeButtons)

        val thId = when (sharedPreferences?.getInt("themeMode", AppCompatDelegate.MODE_NIGHT_NO)) {
            AppCompatDelegate.MODE_NIGHT_YES -> {
                R.id.darkTheme
            }
            AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM -> {
                R.id.systemTheme
            }
            else -> {
                R.id.lightTheme
            }
        }

        themeButton.check(thId)

        themeButton.addOnButtonCheckedListener(object : MaterialButtonToggleGroup.OnButtonCheckedListener {

            override fun onButtonChecked(
                group: MaterialButtonToggleGroup?,
                checkedId: Int,
                isChecked: Boolean
            ) {
                if (isChecked) {

                    val theme = when (checkedId) {
                        R.id.systemTheme -> AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM
                        R.id.darkTheme -> AppCompatDelegate.MODE_NIGHT_YES
                        else -> AppCompatDelegate.MODE_NIGHT_NO
                    }

                    AppCompatDelegate.setDefaultNightMode(theme)

                    editor?.apply {
                        putInt("themeMode", theme)
                    }?.apply()

                }
            }

        })

    }

}