package com.example.sharedprefsviewpager.fragments

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.sharedprefsviewpager.*
import com.example.sharedprefsviewpager.adapters.RecyclerViewAdapter
import com.example.sharedprefsviewpager.model.Notes
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton
import com.google.android.material.snackbar.Snackbar
import java.lang.Exception

class NoteFragment: Fragment (R.layout.fragment_note), RecyclerViewAdapter.OnItemClickListener {

    private lateinit var buttonAddNote: ExtendedFloatingActionButton
    private lateinit var textViewNoteTitle: TextView
    private lateinit var textViewNote: TextView
    private lateinit var imageViewMore: ImageView

    private lateinit var noteArrayList: ArrayList<Notes>

    companion object {
        lateinit var dbHandler: DbHandler
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        init(view)

        buttonAddNote = view.findViewById(R.id.buttonAddNote)
        buttonAddNote.setOnClickListener {
            startActivity(Intent(activity, NoteActivity::class.java))
        }

        dbHandler = DbHandler(requireActivity(), null, null, 1)
        viewNotes()

        imageViewMore = view.findViewById(R.id.imageViewMore)
        imageViewMore.setOnClickListener {

            MaterialAlertDialogBuilder(requireActivity(),
                R.style.ThemeOverlay_MaterialComponents_MaterialCalendar_Fullscreen)
                .setMessage("Do you really want to delete all notes?")
                .setTitle("Delete all notes")
                .setPositiveButton("Yes") { dialog, which ->
                    dbHandler.writableDatabase.execSQL("delete from note")
                    viewNotes()
                    Snackbar.make(requireView(), "Notes deleted successfully", Snackbar.LENGTH_SHORT).show()
                }
                .setNegativeButton("No") { dialog, which ->

                }.show()

        }

        noteArrayList = arrayListOf()

        val item = object : SwipeToDelete(requireActivity(), 0, ItemTouchHelper.RIGHT) {
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                Log.d("MY_TAG", viewHolder.adapterPosition.toString())
                val dbHandler = DbHandler(requireActivity(), null, null, 1)

                onItemSwipe(viewHolder.adapterPosition)
                viewNotes()
                Snackbar.make(requireView(), "Note deleted successfully", Snackbar.LENGTH_SHORT).show()
            }
        }

        val itemTouchHelper = ItemTouchHelper(item)
        itemTouchHelper.attachToRecyclerView(requireView().findViewById(R.id.recyclerView))

    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        val inflater = requireActivity().menuInflater
        inflater.inflate(R.menu.note_menu, menu)
    }

    private fun init(view: View) {

        try {
            textViewNoteTitle = view.findViewById(R.id.textViewNoteTitle)
            textViewNote = view.findViewById(R.id.textViewNote)
        } catch (e: Exception) {
            Log.d("MY_TAG", e.message.toString())
        }

    }

    private fun viewNotes() {

        val noteList = dbHandler.getNotes(requireActivity())
        val adapter = RecyclerViewAdapter(requireActivity(), noteList, this)
        val recyclerView: RecyclerView = requireView().findViewById(R.id.recyclerView)
        recyclerView.layoutManager = LinearLayoutManager(activity, RecyclerView.VERTICAL, false)
        recyclerView.adapter = adapter

        noteArrayList = dbHandler.getNotes(requireActivity())

    }

    override fun onResume() {
        super.onResume()
        viewNotes()
    }

    override fun onItemClick(position: Int) { // new
        val note = noteArrayList[position]

        val intent = Intent(activity, EditNoteActivity::class.java)
        intent.putExtra("title", note.title.toString())
        intent.putExtra("note", note.note.toString())
        intent.putExtra("id", note.id)
        startActivity(intent)
    }

    override fun onItemSwipe(position: Int) {
        val note = noteArrayList[position]
        val id = note.id
        Log.d("MY_TAG", "ID: $id")
        dbHandler.writableDatabase.delete(DbHandler.TABLE_NAME, "nId=?", arrayOf(id.toString()))
    }

}