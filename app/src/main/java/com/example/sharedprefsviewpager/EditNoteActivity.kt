package com.example.sharedprefsviewpager

import android.content.ContentValues
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.example.sharedprefsviewpager.databinding.ActivityEditNoteBinding

class EditNoteActivity : AppCompatActivity() {

    private lateinit var binding: ActivityEditNoteBinding

    private var note: String? = null
    private var title: String? = null
    private var id: Int? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityEditNoteBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val extras: Bundle? = intent.extras
        note = extras?.getString("note", "")
        title = extras?.getString("title", "")
        id = extras?.getInt("id")

        Log.d("MY_TAG", id.toString())
        binding.editTextNote.setText(note)
        binding.editTextTitle.setText(title)

        binding.imageViewBack.setOnClickListener {
            startActivity(Intent(this, MainActivity::class.java))
            finish()
        }

    }

    private fun saveValue() {
        val noteText = binding.editTextNote.text.toString()
        val noteTitle = binding.editTextTitle.text.toString()

        val dbHandler = DbHandler(this, null, null, 1)

        val values = ContentValues()
        values.put(DbHandler.COLUMN_TITLE, noteTitle)
        values.put(DbHandler.COLUMN_NOTE, noteText)

        dbHandler.writableDatabase.update(DbHandler.TABLE_NAME, values, "nId=?", arrayOf(id.toString()))
        Log.d("MY_TAG", "NEW_ID: $id")
    }

    override fun onPause() {
        super.onPause()
        if (binding.editTextNote.text.isNotEmpty() || binding.editTextTitle.text.isNotEmpty()) {
            saveValue()
        }
    }

}