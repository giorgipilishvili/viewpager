package com.example.sharedprefsviewpager.model

import android.content.Context
import android.content.LocusId

data class Notes(
    var id: Int = 0,
    var title: String? = null,
    var note: String? = null
)