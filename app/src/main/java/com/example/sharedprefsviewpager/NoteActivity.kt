package com.example.sharedprefsviewpager

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.WindowManager
import com.example.sharedprefsviewpager.databinding.ActivityNoteBinding
import com.example.sharedprefsviewpager.fragments.NoteFragment
import com.example.sharedprefsviewpager.model.Notes

class NoteActivity : AppCompatActivity() {

    private lateinit var binding: ActivityNoteBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityNoteBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.imageViewBack.setOnClickListener {
            startActivity(Intent(this, MainActivity::class.java))
            finish()
        }

        binding.editTextNote.requestFocus()
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE)
    }

    private fun saveValue() {
        val noteText = binding.editTextNote.text.toString()
        val noteTitle = binding.editTextTitle.text.toString()

        val note = Notes()
        note.title = noteTitle
        note.note = noteText
        NoteFragment.dbHandler.addNote(this, note)
    }

    override fun onPause() {
        super.onPause()
        if (binding.editTextNote.text.isNotEmpty() || binding.editTextTitle.text.isNotEmpty()) {
            saveValue()
        }
    }

}