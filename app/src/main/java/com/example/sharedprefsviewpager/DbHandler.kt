package com.example.sharedprefsviewpager

import android.annotation.SuppressLint
import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log
import com.example.sharedprefsviewpager.model.Notes
import java.lang.Exception

class DbHandler(context: Context, name: String?, factory: SQLiteDatabase.CursorFactory?, version: Int):
    SQLiteOpenHelper(context, DATABASE_NAME, factory, DATABASE_VERSION) {

    companion object {
        private val DATABASE_NAME = "NoteData.db"
        private val DATABASE_VERSION = 1

        val TABLE_NAME = "note"
        val COLUMN_ID = "nId"
        val COLUMN_TITLE = "title"
        val COLUMN_NOTE = "note"
    }

    override fun onCreate(db: SQLiteDatabase?) {
        val CREATE_NOTE_TABLE = ("create table " + TABLE_NAME + "(" +
                COLUMN_ID + " integer primary key autoincrement, " +
                COLUMN_TITLE + " text, " +
                COLUMN_NOTE + " text" + ")")
        db?.execSQL(CREATE_NOTE_TABLE)

//        db?.execSQL("")
    }

    override fun onUpgrade(p0: SQLiteDatabase?, p1: Int, p2: Int) {
        TODO("Not yet implemented")
    }

    @SuppressLint("Range")
    fun getNotes(context: Context): ArrayList<Notes> {
        val qry = "SELECT * From $TABLE_NAME"
        val db = this.readableDatabase
        val cursor = db.rawQuery(qry, null)
        val notes = ArrayList<Notes>()

        if (cursor.count == 0) {
            Log.d("MY_TAG", "notes not found.")
        } else {
            while (cursor.moveToNext()) {
                val note = Notes()
                note.id = cursor.getInt(cursor.getColumnIndex(COLUMN_ID))
                note.title = cursor.getString(cursor.getColumnIndex(COLUMN_TITLE))
                note.note = cursor.getString(cursor.getColumnIndex(COLUMN_NOTE))
                notes.add(note)
            }
        }
        cursor.close()
        db.close()
        return notes
    }

    fun addNote(context: Context, note: Notes) {
        val cursor = this.readableDatabase.rawQuery("select * from " + TABLE_NAME + " where " + COLUMN_TITLE + "=" + "'" + note.title + "'", null)

        if (!cursor.moveToNext()) {
            val values = ContentValues()
            values.put(COLUMN_TITLE, note.title)
            values.put(COLUMN_NOTE, note.note)
            val db = this.writableDatabase
            try {
                db.insert(TABLE_NAME, null, values)
                Log.d("MY_TAG", "Note Add Success.")
            } catch (e : Exception) {
                Log.d("MY_TAG", e.message.toString())
            }
            db.close()
        }
    }

}