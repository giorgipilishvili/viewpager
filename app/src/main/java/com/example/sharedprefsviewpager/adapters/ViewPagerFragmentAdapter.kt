package com.example.sharedprefsviewpager.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.sharedprefsviewpager.fragments.GalleryFragment
import com.example.sharedprefsviewpager.fragments.SettingsFragment
import com.example.sharedprefsviewpager.fragments.NoteFragment

class ViewPagerFragmentAdapter(activity: FragmentActivity): FragmentStateAdapter(activity) {
    override fun getItemCount() = 3

    override fun createFragment(position: Int): Fragment {
        return when (position) {
            0 -> NoteFragment()
            1 -> GalleryFragment()
            2 -> SettingsFragment()
            else -> NoteFragment()
        }
    }
}