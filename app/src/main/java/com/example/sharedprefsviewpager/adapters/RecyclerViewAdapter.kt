package com.example.sharedprefsviewpager.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.sharedprefsviewpager.R
import com.example.sharedprefsviewpager.model.Notes
import com.google.android.material.card.MaterialCardView

class RecyclerViewAdapter(context: Context, val notes: ArrayList<Notes>, private val listener: OnItemClickListener): RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>() {

    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView), View.OnClickListener { // new (View.OnClickListener)
        val noteTitle = itemView.findViewById<TextView>(R.id.textViewNoteTitle)
        val note = itemView.findViewById<TextView>(R.id.textViewNote)

        private val item: MaterialCardView = itemView.findViewById(R.id.item_note) // new

        init { // new
            item.setOnClickListener(this)
        }

        override fun onClick(p0: View?) { // new
            val position: Int = adapterPosition
            if (position != RecyclerView.NO_POSITION) {
                listener.onItemClick(position)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_note, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val note: Notes = notes[position]
        holder.note.text = note.note
        holder.noteTitle.text = note.title

        if (holder.note.text.isEmpty()) {
            holder.note.text = "No Description"
        }
        if (holder.noteTitle.text.isEmpty()) {
            holder.noteTitle.text = "No Title"
        }
    }

    override fun getItemCount(): Int = notes.size

    interface OnItemClickListener { // new
        fun onItemClick(position: Int)
        fun onItemSwipe(position: Int)
    }

}