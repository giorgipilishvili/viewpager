package com.example.sharedprefsviewpager

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.app.AppCompatDelegate
import com.example.sharedprefsviewpager.adapters.ViewPagerFragmentAdapter
import com.example.sharedprefsviewpager.databinding.ActivityMainBinding
import com.google.android.material.tabs.TabLayoutMediator

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var viewPagerFragmentAdapter: ViewPagerFragmentAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        /* ის თემა დაყენდეს რომელიც sharedPreferences-შია შენახული. default-ად light-ი */

        val sharedPreferences = getSharedPreferences("Theme", Context.MODE_PRIVATE)

        val theme = sharedPreferences.getInt("themeMode", AppCompatDelegate.MODE_NIGHT_NO)

        AppCompatDelegate.setDefaultNightMode(theme)

        viewPagerFragmentAdapter = ViewPagerFragmentAdapter(this)
        binding.viewPager.adapter = viewPagerFragmentAdapter

        TabLayoutMediator(binding.viewPagerTabs, binding.viewPager) { tab, position ->
            when (position) {
                0 -> tab.setIcon(R.drawable.ic_baseline_edit_note_24)
                1 -> tab.setIcon(R.drawable.ic_baseline_image_24)
                2 -> tab.setIcon(R.drawable.ic_baseline_settings_24)
            }
        }.attach()

    }

}